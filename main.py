import subprocess

# The generated Flutter code
code = 'Text("Hello, World!")'

# Run the Dart script
subprocess.run(['../../flutter/bin/dart', 'run_code.dart', code], cwd='./screenshot_app/lib/')
