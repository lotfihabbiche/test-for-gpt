import 'dart:io';

void main(List<String> arguments) {
  // The Flutter code to run
  String code = arguments[0];

  // The Flutter app template
  String app = '''
  import 'package:flutter/material.dart';

  void main() {
    runApp(MyApp());
  }

  class MyApp extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
      return MaterialApp(
        home: Scaffold(
          body: $code,
        ),
      );
    }
  }
  ''';

  // Write the app to a file
  File('./main.dart').writeAsStringSync(app);

  // Run the app
  Process.runSync('../../flutter/bin/flutter', ['run']);

  // Take a screenshot (this requires the screenshot package)
  Process.runSync('../../flutter/bin/flutter',
      ['screenshot', '--out', 'screenshots/screenshot.png']);
}
